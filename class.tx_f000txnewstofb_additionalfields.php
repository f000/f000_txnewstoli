
<?php
// Pochustutu
class tx_f000txnewstofb_additionalFields implements tx_scheduler_AdditionalFieldProvider {
    public function getAdditionalFields(array &$taskInfo, $task, tx_scheduler_Module $parentObject) {
        
        
        $pxanewstofbSocialPublisherConfig = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'tx_f000txnewstofb_config_social_publishing', 'hidden=0
                AND (starttime=0 OR starttime<=' . $GLOBALS['EXEC_TIME'] . ')');
        
        $fieldCodeCfgRec = '';
        /////////////////////
        $fieldCodeCfgRecPublisher = '';
        
        if (empty($taskInfo['f000txnewstofbconfiguration'])) {
            if($parentObject->CMD == 'edit') {
                $taskInfo['f000txnewstofbconfiguration'] = $task->f000txnewstofbconfiguration;
            } else {
                $taskInfo['f000txnewstofbconfiguration'] = '';
            }
        }

        $cfgValues =  explode(",", $taskInfo['f000txnewstofbconfiguration']);

        /////////////////////////////////////
        foreach ($pxanewstofbSocialPublisherConfig as $cfgRec) {
          $fieldCodeCfgRec .= '<option value="'.$cfgRec['uid'].'" ' . (in_array($cfgRec['uid'], $cfgValues)?'selected':''). '>' . $cfgRec['title'] . '</option>';
        }

        
        $fieldID = 'task_f000txnewstofbconfiguration';
        $fieldCode = '<select name="tx_scheduler[f000txnewstofbconfiguration][]" id="' . $fieldID . '" multiple="multiple">' . $fieldCodeCfgRec . '</select>';
        $additionalFields[$fieldID] = array(
            'code'     => $fieldCode,
            'label'    => 'LLL:EXT:f000_txnewstofb/locallang_db.xml:scheduler.tx_f000txnewstofb_configuration'
        );
        
        return $additionalFields;
    }

    public function validateAdditionalFields(array &$submittedData, tx_scheduler_Module $parentObject) {
        $submittedData['f000txnewstofbconfiguration'] = trim(implode(",", $submittedData['f000txnewstofbconfiguration']));
        return true;
    }

    public function saveAdditionalFields(array $submittedData, tx_scheduler_Task $task) {
        $task->f000txnewstofbconfiguration = $submittedData['f000txnewstofbconfiguration'];
    }
}
?>