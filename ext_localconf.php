<?php
if (!defined ('TYPO3_MODE'))     die ('Access denied.');

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_f000txnewstofb_publish'] = array (
    'extension'        => $_EXTKEY,
    'title'            => 'LLL:EXT:' . $_EXTKEY . '/locallang_db.xml:scheduler.tx_f000txnewstofb_title',
    'description'      => 'LLL:EXT:' . $_EXTKEY . '/locallang_db.xml:scheduler.tx_f000txnewstofb_description',
    'additionalFields' => 'tx_f000txnewstofb_additionalfields'
);

?>
