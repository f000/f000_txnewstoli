<?php

########################################################################
# Extension Manager/Repository config file for ext "f000_txnewstofb".
#
# Auto generated 30-05-2013 17:22
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
    'title' => 'News to Facebook Integration',
    'description' => 'Publish news from tx_news in TYPO3 to Facebook wall.',
    'category' => 'be',
    'shy' => 0,
    'version' => '2.0.0',
    'dependencies' => 'news',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearcacheonload' => 0,
    'lockType' => '',
    'author' => 'Lukas Vorlicek, CodeArt',
    'author_email' => 'lukas.vorlicek@codeart.cz',
    'author_company' => 'www.codeart.cz',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.0-0.0.0',
            'news' => '',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
    '_md5_values_when_last_written' => '',
    'suggests' => array(
    ),
);

?>