<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$tempColumns = array (
	'tx_f000txnewstofb_dont_publish' => array (		
		'exclude' => 1,		
		'label' => 'LLL:EXT:f000_txnewstofb/locallang_db.xml:tx_f000txnewstofb_dont_publish',		
		'config' => array (
			'type' => 'check',
		)
	),
	'tx_f000txnewstofb_published' => array (		
		'exclude' => 1,		
		'label' => 'LLL:EXT:f000_txnewstofb/locallang_db.xml:tx_f000txnewstofb_published',		
		'config' => array (
			'type' => 'check',
		)
	),
);

t3lib_div::loadTCA('tx_news_domain_model_news');
t3lib_extMgm::addTCAcolumns('tx_news_domain_model_news', $tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('tx_news_domain_model_news', '--div--;Pxa News to Facebook,tx_f000txnewstofb_dont_publish,tx_f000txnewstofb_published;;;;1-1-1');

t3lib_extMgm::allowTableOnStandardPages('tx_f000txnewstofb_config_app');
$TCA['tx_f000txnewstofb_config_app'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:f000_txnewstofb/locallang_db.xml:tx_f000txnewstofb_config_app',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'type' => 'type',
		'default_sortby' => 'ORDER BY crdate',
		'enablecolumns' => array(
			'disabled' => 'hidden'
		),
    'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY).'res/img/facebook.gif',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php'
	)
);

$TCA['tx_f000txnewstofb_app_token'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:f000_txnewstofb/locallang_db.xml:tx_f000txnewstofb_app_token',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'type' => 'type',
		'default_sortby' => 'ORDER BY crdate',
    'readOnly' => true,
    'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY).'res/img/facebook.gif',
    'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php'
	)
);

t3lib_extMgm::allowTableOnStandardPages('tx_f000txnewstofb_config_social_publishing');
$TCA['tx_f000txnewstofb_config_social_publishing'] = array(
	'ctrl' => array(
		'title' => 'LLL:EXT:f000_txnewstofb/locallang_db.xml:tx_f000txnewstofb_config_social_publishing',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'type' => 'type',
		'default_sortby' => 'ORDER BY crdate',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime'
		),
		'requestUpdate' => 'appid',
    'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY).'res/img/facebook.gif',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php'
	)
);
?>