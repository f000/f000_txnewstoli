<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Lukas Vorlicek <lukas.vorlicek@codeart.cz>
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
* News Storage Class
*/
class News {
    
    public $newsStorage = array();
    public $inCat;
    public $table;
    public $where;
    public $newsUid;
    public $host;

    /**
    * Initialize TSFE and select raw news records for publishing
    *
    * @param integer 
    * @param integer  
    * @param string
    * @param integer
    */
    function __construct($detailNewsPid,$storagePid,$tagId,$descriptionLength) {

        // Making an instance of frontend classes, tsfe
        $this->cObj = t3lib_div::makeInstance('tslib_cObj');

        $temp_TTclassName = t3lib_div::makeInstance('t3lib_timeTrack');
        $GLOBALS['TT'] = new $temp_TTclassName();
        $GLOBALS['TT']->start();
        
        $feUserObj = \TYPO3\CMS\Frontend\Utility\EidUtility::initFeUser();
        
        $GLOBALS['TSFE'] = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $TYPO3_CONF_VARS, $detailNewsPid, 0, 0);
        $GLOBALS['TSFE']->connectToDB();
        $GLOBALS['TSFE']->fe_user = $feUserObj;
        $GLOBALS['TSFE']->id = $pid;
        $GLOBALS['TSFE']->determineId();
        $GLOBALS['TSFE']->getCompressedTCarray();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->getConfigArray();
        $GLOBALS['TSFE']->includeTCA();

        // preparing csConvObj
        if (!is_object($GLOBALS['TSFE']->csConvObj)) {
            if (is_object($GLOBALS['LANG'])) {
                $GLOBALS['TSFE']->csConvObj = $GLOBALS['LANG']->csConvObj;
            } else {
                $GLOBALS['TSFE']->csConvObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Charset\\CharsetConverter');
            }
        }
        // preparing renderCharset
        if (!is_object($GLOBALS['TSFE']->renderCharset)) {
            if (is_object($GLOBALS['LANG'])) {
                $GLOBALS['TSFE']->renderCharset = $GLOBALS['LANG']->charSet;
            } else {
                $GLOBALS['TSFE']->renderCharset = $GLOBALS['TYPO3_CONF_VARS']['BE']['forceCharset'];
            }
        }
        
        $this->cObj->start(array(),'');
        
        $this->fileRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\FileRepository');
        
        $this->newsUid = '';
        $this->inCat = '';
        $this->table = 'tx_news_domain_model_news';
        $this->where = 'tx_f000txnewstofb_published = 0  AND tx_f000txnewstofb_dont_publish = 0 ' . $this->enableFields('tx_news_domain_model_news').  ($storagePid ?' AND pid = ' .intval($storagePid) :'');
        
        // Get and validate the list of selected news tags
        $selectedNewsTags = array();
        $selectedNewsTags = t3lib_div::intExplode(',', $tagId, TRUE);
        foreach($selectedNewsTags as $key => $value) {
            if($value < 1) {
                unset($selectedNewsTags[$key]);
            }
        }
        
        if(count($selectedNewsTags) > 0) {
            $this->table = 'tx_news_domain_model_news LEFT JOIN tx_news_domain_model_news_tag_mm ON tx_news_domain_model_news.uid = tx_news_domain_model_news_tag_mm.uid_local';    
            $this->inCat = ' AND uid_foreign IN (' . implode(',', $selectedNewsTags) .') ';
            $this->where = 'tx_f000txnewstofb_published = 0  AND tx_f000txnewstofb_dont_publish = 0 ' . $this->inCat . $this->enableFields('tx_news_domain_model_news') . ($fbAppSettings['storagePid']?' AND pid = ' .intval($fbAppSettings['storagePid']) :'');
        }
        
        $this->where .= ' AND sys_language_uid < 1 '; //TODO
            
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('tx_news_domain_model_news.*', $this->table, $this->where, 'tx_news_domain_model_news.uid');
       
        while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)){
            $curNews= $this->setNews($row, $detailNewsPid,$descriptionLength);
            array_push($this->newsStorage,$curNews);
        }
    }

    /**
    * Prepare single news for publishing
    *
    * @param array
    * @param integer
    * @param integer
    * @return array
    */
    private function setNews($curRow, $detailNewsPid, $description){
        $article = Array(
            "name" => $curRow['title']
        );

        if(intval($description) > 0) {
            $desc = strip_tags($curRow['teaser'] ? $curRow['teaser'] : $curRow['bodytext']);
            $desc = preg_replace('/\s+/', ' ', $desc);
            $desc = $this->cObj->crop($desc, intval($description) . '|...|1');
            $article['description'] = $desc;
        }
        if($curRow['fal_media']) {
            $fileObjects = $this->fileRepository->findByRelation('tx_news_domain_model_news', 'fal_media', $curRow['uid']);
            foreach ($fileObjects as $key => $img) {
                $prop = $img->getReferenceProperties();
                if ( $prop['showinpreview'] ){
                    $article['picture'] = $img->getOriginalFile()->getPublicUrl();
                    break;
                }
            }
        }

        $article['uid']= $curRow['uid'];
        return $article;
    }

    /**
    * Return prepared news records
    *
    * @return array
    */
    public function getNews() {
        return $this->newsStorage;
    }

    /**
    * Implements enableFields call that can be used from regular FE and eID
    *
    * @param string $tableName
    * @return string
    */
    public function enableFields($tableName) {

        if ($GLOBALS['TSFE']) {
            return $this->cObj->enableFields($tableName);
        }
        $sys_page = t3lib_div::makeInstance('t3lib_pageSelect');

        /* @var $sys_page t3lib_pageSelect */
        return $sys_page->enableFields($tableName);
    }

}
?>