<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

define('TYPO3_MOD_PATH', '../typo3conf/ext/f000_txnewstofb/wizards/');


require_once( '../src/Facebook/HttpClients/FacebookHttpable.php' );
require_once( '../src/Facebook/HttpClients/FacebookCurl.php' );
require_once( '../src/Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( '../src/Facebook/Entities/AccessToken.php' );
require_once( '../src/Facebook/Entities/SignedRequest.php' );
require_once( '../src/Facebook/FacebookSession.php' );
require_once( '../src/Facebook/FacebookRedirectLoginHelper.php' );
require_once( '../src/Facebook/FacebookRequest.php' );
require_once( '../src/Facebook/FacebookResponse.php' );
require_once( '../src/Facebook/FacebookSDKException.php' );
require_once( '../src/Facebook/FacebookRequestException.php' );
require_once( '../src/Facebook/FacebookOtherException.php' );
require_once( '../src/Facebook/FacebookClientException.php' );
require_once( '../src/Facebook/FacebookServerException.php' );
require_once( '../src/Facebook/FacebookAuthorizationException.php' );
require_once( '../src/Facebook/GraphObject.php' );
require_once( '../src/Facebook/GraphPage.php' );
require_once( '../src/Facebook/GraphUserPage.php' );
require_once( '../src/Facebook/GraphSessionInfo.php' );

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookOtherException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\GraphUserPage;       
use Facebook\GraphPage;

require ('../../../../typo3/init.php');
//require ('../../../../typo3/template.php');


session_start();

$uid = $_GET["P"]['uid'] ? $_GET["P"]['uid'] : $_GET['uid'];
$pid = $_GET["P"]['pid'] ? $_GET["P"]['pid'] : $_GET['pid'];

$row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'tx_f000txnewstofb_config_app', 'uid = ' . intval($uid));

$appId = $row['0']['appid'];
$appSecret = $row['0']['secret'];
$pageId = $row['0']['weburl'];
$permissionScope = Array('manage_pages, read_stream, publish_actions');

FacebookSession::setDefaultApplication($appId,$appSecret);
$redirectUrl = $_SERVER['SCRIPT_URI'] .'?' . 'uid='.$uid.'&pid='.$pid;
$urlHelper = new FacebookRedirectLoginHelper( $redirectUrl );
$page_token = false;

if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
    // create new session from saved access_token
    $session = new FacebookSession( $_SESSION['fb_token'] );
    // validate the access_token to make sure it's still valid
    try {
        if ( !$session->validate() ) {
            $session = null;
        }
    }
    catch ( Exception $e ) {
        $session = null;
    }
} 


if ( !isset( $session ) || $session === null ) {
    // no session exists
    try {
        $session = $urlHelper->getSessionFromRedirect();
    }
    catch ( FacebookRequestException $ex ) {
        print_r( $ex );
    }
    catch ( Exception $ex ) {
        print_r( $ex );
    }
  
}

if ( isset( $session ) ) {
    $session->getLongLivedSession();
    $pages = (new FacebookRequest(
    $session, 'GET', '/me/accounts'
        ))->execute()->getGraphObject()->asArray();
    foreach ( $pages['data'] AS $page){
        if ( $page->id == $pageId ){
            $page_token = $page->access_token;
            break; 
        }
    }
}

$loginUrl = $urlHelper->getLoginUrl( $permissionScope );

if ( $page_token ){
    $rowKey = array(
        'access_token' => $page_token,
        'parent' => $uid,
        'pid' => $pid,
        'appid' => $appId,
        'title' => $pageId
    );
    $res= $GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_f000txnewstofb_app_token',$rowKey);
    $usermsg = "Token ".$page_token ;
}

?>
<!doctype html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <link rel="stylesheet" type="text/css" href="wizard.css">
    <title>php-sdk</title>
    <style>
    body {
        font-family: 'Lucida Grande', Verdana, Arial, sans-serif;
    }
    h1 a {
        text-decoration: none;
        color: #3b5998;
    }
    h1 a:hover {
        text-decoration: underline;
    }
    </style>
</head>
<body>
    <h1>Connect with Faceboook</h1>
    <a href="<?php echo $loginUrl; ?>" class="button">Connect to Facebook and get Page Tocken</a>
    <br /><br />
    <p><?php echo $usermsg;?></p>
</body>
</html>


