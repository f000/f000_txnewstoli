<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2014 Lukas Vorlicek <lukas.vorlicek@codeart.cz>
*  
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/HttpClients/FacebookHttpable.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/HttpClients/FacebookCurl.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/HttpClients/FacebookCurlHttpClient.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/Entities/AccessToken.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/Entities/SignedRequest.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookSession.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookRedirectLoginHelper.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookRequest.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookResponse.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookSDKException.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookRequestException.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookOtherException.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookClientException.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookServerException.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/FacebookAuthorizationException.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/GraphObject.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/GraphPage.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/GraphUserPage.php') );
require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'src/Facebook/GraphSessionInfo.php') );

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookOtherException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\GraphUserPage;       
use Facebook\GraphPage;

require_once( t3lib_extMgm::extPath('f000_txnewstofb', 'lib/class.tx_f000txnewstofb_news.php'));

define(PUBLISH, 1);

class tx_f000txnewstofb_publish extends tx_scheduler_Task {

    public $News; 
    public $facebook;
    public $messages;
    public $FbNews;
    
    /**
    * Executes the schedule task to post news to facebook
    *
    * @return boolean
    */
    public function execute() {

        $config=$this->GetPublishConf();
        #print_r($config);
        $this->News = new News($config['detailnewspid'],$config['storagepid'],$config['tagid'],$config['desclength']);
        $News = $this->News->getNews();
        #print_r($News);
        $this->FbNews= $this->PrepareForPublish($News,$config);
        #print_r($this->FbNews);
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'tx_f000txnewstofb_config_app', 'uid = ' . intval($config['appid']));
        $publishLog = array();
        if($row) {
            FacebookSession::setDefaultApplication( $row['0']['appid'], $row['0']['secret'] );
            $publishLog = $this->Publish($this->FbNews,$config['pageid'],$publishLog);
        }
        else {
            array_push($publishLog, 'ERR:No associated Facebook Application record found.');
        }      
        $this->writePublishLog($publishLog, $config['logfilepath']);

        return TRUE;
    }


    /**
    * Get publish config
    *
    * @return array
    */ 
    private  function GetPublishConf(){
        global $GLOBALS, $TSFE, $TYPO3_CONF_VARS;
        $configUid =  t3lib_div::trimExplode(',', $this->f000txnewstofbconfiguration, TRUE);
        $ConfigTemp = array();

        foreach($configUid as $uid) {
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('appid,pageid,weburl,detailnewspid,storagepid,tagid,desclength,logfilepath,defaultimagepath', 'tx_f000txnewstofb_config_social_publishing', 'hidden=0
                AND uid = ' . intval($uid));
            if($row['0']) $ConfigTemp = $row['0'];
        }

        foreach($ConfigTemp as $key => $value) {
            $config[$key]= $value;
        }
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'tx_f000txnewstofb_app_token', 'uid = ' . intval($config['pageid']));

        $config['access_token'] = $row['0']['access_token'];
        $config['pageid'] = $row['0']['title'];
      
        return $config;
    }

    /**
    * Prepare each news record for publishing. Add typolink, fix absolute paths, add Access Token
    *
    * @param array
    * @param array
    * @return array
    */ 
    private function PrepareForPublish($newsSrc,$fbconfig){
        $publishNews = array();
        $cObj = t3lib_div::makeInstance('tslib_cObj');
        foreach ($newsSrc as $index => $news) {
            $publishNews[$index] = $news;   
            $parameters = "&tx_news_pi1[news]=".$news['uid']."&tx_news_pi1[controller]=News&tx_news_pi1[action]=detail";
            $publishNews[$index]['link'] = $fbconfig['weburl'] . $cObj->typoLink_URL(array('parameter' => $fbconfig['detailnewspid'],'additionalParams' => $parameters, 'forceAbsoluteUrl' => 0));
            if (!empty($news['picture'])){
                $publishNews[$index]['picture'] = $fbconfig['weburl'] . $news['picture'];
            }
            else {
                $publishNews[$index]['picture'] = $fbconfig['weburl'] . $fbconfig['defaultimagepath'];
            }
            $publishNews[$index]['access_token'] = $fbconfig['access_token'];
            //       $publishNews[$index]['caption'] = $fbconfig['weburl'];
        }
    
        return $publishNews;
    }

    /**
    * Publish prepared news records to Facebook using FB API
    *
    * @param array
    * @param integer 
    * @param array
    * @return array
    */      
    private function Publish($publishSrc,$pageId,$publishLog = array()){ 
        try{
            foreach ($publishSrc as $data) {
                $uid = $data['uid'];
                unset($data['uid']);
                $session = new FacebookSession($data['access_token']);
                $post = (new FacebookRequest(
                $session, 'POST', '/'. $pageId .'/feed', $data
                    ))->execute();   
                array_push($publishLog, 'Record news uid - ' . $uid. ' was published.');
                $this->publishStutus($uid,PUBLISH);

            }
        }
        catch (FacebookRequestException $e){  
            array_push($publishLog, $e);
            echo $e;
        }
        catch (Exception $e) {
            array_push($publishLog, $e);
            echo $e;
        }
        return $publishLog;
    }
    
    /**
    * Write array of errors to logfile
    *
    * @param array
    * @param string
    */
    private function writePublishLog($publishLog,$logFilePath){
        if ($logFilePath) {
            $logFilePath = t3lib_div::getFileAbsFileName($logFilePath);
        }
        $logFile = fopen($logFilePath, 'a');
        if($logFile) {
            $time = date('d-M-Y H:i:s');
            foreach( $publishLog as $key => $val ) {
                fprintf($logFile, "%s %s \n", $time, $val);
            }
            fclose($logFile);
        }
    }
    
    /**
    * Set published news as published
    *
    * @param integer
    * @param integer
    */
    public function publishStutus($newsUid,$status){
        $sqlUpdate = 'UPDATE tx_news_domain_model_news SET tx_f000txnewstofb_published = '.$status.' WHERE uid in (' . $newsUid .')';
        $GLOBALS['TYPO3_DB']->sql_query($sqlUpdate);
    }
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/f000_txnewstofb/class.tx_f000txnewstofb_publish.php']) {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/f000_txnewstofb/class.tx_f000txnewstofb_publish.php']);
}

?>